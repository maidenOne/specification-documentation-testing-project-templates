
application:

test:
	cd code/test/src \
	gcc test_main.c -o test_main -lcmocka

graph:
	cd img; \
	  dot -Tsvg documentStructure.dot -o documentStructure.svg

docs:
	doxygen doc/Doxygen.conf

PHONY: docs
