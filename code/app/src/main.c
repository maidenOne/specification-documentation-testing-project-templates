/** \file main.c
 *  @brief Function prototypes for the console driver.
 *
 *  This contains the prototypes for the console
 *  driver and eventually any macros, constants,
 *  or global variables you will need.
 *
 *  @author Harry Q. Bovik (hqbovik)
 *  @author Fred Hacker (fhacker)
 *  @bug No known bugs.
 */

/**
 *	this is just an documentation example
*/

/*! \brief Brief description.
 *         Brief description continued.
 *
 *  Detailed description starts here.
 */

#include <stdio.h>

int main()
{
	// its very important to initzilize!
	int i=0;
	int j=5;

	// doing some math
	for ( i=0; i<10; i++)
	{
		j = i - j;
	}

	/*! returning 0 */
	return 0;
}
