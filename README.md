# specification-documentation-testing-project-templates

This projects aims to implement a set of "getting started" templates for various languages.<br>
The templates shall contain a well specified, tested, documented and implementad case that<br>
shows the features and benefit of a well structured project.<br>

![alt text](https://gitlab.com/maidenOne/specification-documentation-testing-project-templates/raw/master/img/documentStructure.svg)

Graph above highlights the importance to test against the requirements, not the implementation,<br>
If you test against the implementation, you risk ending up in a situation where you only test code delta.<br>
Experienced programmers will be familiar with the following case: <br>

`Code changed -> test failed -> update test, repeat.`

With the proposed setup you will write code that implements RS_01 according to DS_01. <br>
This is verified by TI_01 that is implemented according to TS_01 to verify RS_01. <br><br>

## WoW (Way of Working)

1.  Specify what you want to implement, what problem do you want to solve? what is the edge cases? This will be your Requirement Specification
2.  Specify in detail the edgecases and how they should be tested to verify your requirement specification. This will be your Test Specification
3.  Specify how you should implement the functionality, keep in maind that it should be possible to test your implementation according to your Test specification.
4.  Implemnt your code and test, at this point it does not really matter what your start with, or if you are a team, different people can implement functionality and tests at the same time.

## Installing dependencies

### Linux (Ubuntu)
  ~~~
  sudo apt install doxygen doxygen-latex libcmocka-dev
  ~~~


### Windows
  TODO

### Mac
  TODO
